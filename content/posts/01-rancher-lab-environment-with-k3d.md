---
title: "Rancher Lab Environment with K3d"
date: 2023-08-14
draft: false
---

### What is Rancher ?

Rancher is a Kubernetes management tool to deploy and run clusters anywhere and on any provider.

Rancher can provision Kubernetes from a hosted provider, provision compute nodes and then install Kubernetes onto them, or import existing Kubernetes clusters running anywhere.

Rancher adds significant value on top of Kubernetes, first by centralizing authentication and role-based access control (RBAC) for all of the clusters, giving global admins the ability to control cluster access from one location.

It then enables detailed monitoring and alerting for clusters and their resources, ships logs to external providers, and integrates directly with Helm via the Application Catalog. If you have an external CI/CD system, you can plug it into Rancher, but if you don't, Rancher even includes Fleet to help you automatically deploy and upgrade workloads.

Rancher is a complete container management platform for Kubernetes, giving you the tools to successfully run Kubernetes anywhere.

🔎 [ranchermanager.docs.rancher.com](https://ranchermanager.docs.rancher.com/)


### What is K3d ?

k3d is a lightweight wrapper to run k3s (Rancher Lab’s minimal Kubernetes distribution) in docker.

k3d makes it very easy to create single- and multi-node k3s clusters in docker, e.g. for local development on Kubernetes.

**_Note:_** k3d is a community-driven project but it’s not an official Rancher (SUSE) product.

{{< alert>}} Requirements ❯
○ [docker](https://docs.docker.com/get-docker/) to be able to use k3d at all &
○ [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl) to interact with the Kubernetes cluster
{{< /alert >}}



🔎 [k3d.io](https://k3d.io/)

If we’re ready, let’s start 🏃

<hr>


### Install K3d

Installation is pretty simple. [Here](https://k3d.io/v5.5.2/#installation) you can continue according to your operating system.

```
➜ brew install k3d
```


### Create K3d Cluster
We have completed the K3d installation. Now it's time to create a cluster. I'm creating a cluster named `racnher-01` with **3** master and **3** worker nodes. I also specify that it expose the ports I need to access Rancher. In K3d, agent refers to the worker node and server refers to the master.


```
➜ k3d cluster create rancher-01 \
  -p "8900:30080@agent:0" -p "8901:30081@agent:0" -p "8902:30082@agent:0" \ 
  --agents 3 \
  --servers 3 \
  --image rancher/k3s:v1.26.7-k3s1
```

I'm setting up cluster in specific version with `v1.26.7-k3s1`. This is because Rancher Stable version (2.7.5) requires kubernetes version < 1.27.0-0.

In a few seconds the cluster was created. Let's see this.

```
➜ k3d cluster list
NAME         SERVERS   AGENTS   LOADBALANCER
rancher-01   3/3       3/3      true
```

You can see all commands related to K3d [here](https://k3d.io/v5.5.2/usage/commands/). It has a very minimal documentation and you can see what you can do with k3d in just a few minutes. If you are reading and applying this article, you can even add a new worker node to the cluster you created by looking at the document :)

Yes, let's take a look at our cluster. 🙋‍♂️


```
➜ kubectl get nodes -o wide 
NAME                      STATUS   ROLES                       AGE   VERSION        INTERNAL-IP   EXTERNAL-IP   OS-IMAGE   KERNEL-VERSION        CONTAINER-RUNTIME
k3d-rancher-01-agent-0    Ready    <none>                      28s   v1.26.7+k3s1   172.21.0.8    <none>        K3s dev    5.15.49-linuxkit-pr   containerd://1.7.1-k3s1
k3d-rancher-01-agent-1    Ready    <none>                      28s   v1.26.7+k3s1   172.21.0.7    <none>        K3s dev    5.15.49-linuxkit-pr   containerd://1.7.1-k3s1
k3d-rancher-01-agent-2    Ready    <none>                      28s   v1.26.7+k3s1   172.21.0.6    <none>        K3s dev    5.15.49-linuxkit-pr   containerd://1.7.1-k3s1
k3d-rancher-01-server-0   Ready    control-plane,etcd,master   65s   v1.26.7+k3s1   172.21.0.3    <none>        K3s dev    5.15.49-linuxkit-pr   containerd://1.7.1-k3s1
k3d-rancher-01-server-1   Ready    control-plane,etcd,master   48s   v1.26.7+k3s1   172.21.0.4    <none>        K3s dev    5.15.49-linuxkit-pr   containerd://1.7.1-k3s1
k3d-rancher-01-server-2   Ready    control-plane,etcd,master   35s   v1.26.7+k3s1   172.21.0.5    <none>        K3s dev    5.15.49-linuxkit-pr   containerd://1.7.1-k3s1
```

Since there seems to be no problem, let's start Rancher installation.

### Deploy Rancher on K3d Cluster

```
➜ helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
➜ helm install rancher rancher-stable/rancher \
    --namespace cattle-system \
    --create-namespace \
    --set ingress.enabled=false \
    --set tls=external \
    --set replicas=3 
```

[Here](https://artifacthub.io/packages/helm/rancher-stable/rancher) you can examine the chart version, values, document etc. The stable version was 2.7.5 when I installed it. Maybe we can look at cluster upgrade in another article :)

Let's check with helm which version is installed.

```
➜ helm ls -a -n cattle-system
NAME           	NAMESPACE    	REVISION	UPDATED                                	STATUS  	CHART                        	APP VERSION
rancher        	cattle-system	1       	2023-08-11 21:58:39.511936 +0300 +03   	deployed	rancher-2.7.5                	v2.7.5
rancher-webhook	cattle-system	1       	2023-08-11 19:07:35.888035127 +0000 UTC	deployed	rancher-webhook-2.0.5+up0.3.5	0.3.5
```

It may take a while for the cluster to stand up. If we are sure that everything is ok with the `cattle-system` and `cattle-fleet-system` namespaces, let's move on to the next step.

### Expose Rancher with NodePort

Create the NodePort service to access Rancher UI.

```
➜ kubectl apply -f - <<EOF
    apiVersion: v1
    kind: Service
    metadata:
      labels:
        app: rancher
      name: ranchernp
      namespace: cattle-system
    spec:
      ports:
      - name: http
        nodePort: 30080
        port: 80
        protocol: TCP
        targetPort: 80
      - name: https-internal
        nodePort: 30081
        port: 443
        protocol: TCP
        targetPort: 443
      selector:
        app: rancher
      type: NodePort
EOF
```


Now we can access the UI with [localhost:8901](https://localhost:8901/). 🐍

![01-welcome-rancher](img/01-rancher/01-welcome-rancher.png)

as it also shows here

```
➜ kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}{{"\n"}}'
```

Let's get the default admin password with the command. then it will ask us to create a new password.

![02-overview](img/01-rancher/02-overview.png)

🔥 Now we can play with Racher inside. First let's take a look at our local cluster.

![03-overview](img/01-rancher/03-cluster.png)

That was the installation part. I am stopping the K3d cluster.

```
➜ k3d cluster stop rancher-01
```

You can delete it if you wish. 🔫

```
➜ k3d cluster delete rancher-01
```

### Resources 🧐

- [Set up K3s in High Availability using k3d](https://www.suse.com/c/rancher_blog/set-up-k3s-in-high-availability-using-k3d/)
- [K3d Guide](https://k3d.io/v5.5.2/usage/configfile/)
- [Install the Rancher Helm Chart](https://ranchermanager.docs.rancher.com/v2.7/pages-for-subheaders/install-upgrade-on-a-kubernetes-cluster#install-the-rancher-helm-chart)
