---
title: "Rename Appset Name in Argo CD"
date: 2024-01-21T06:30:00+03:00
draft: false
description: ""
---


Greetings everyone, I hope you're well. 👋🏻 <br>

It's been a long time since I've posted. In this article, I wanted to write to you about a topic that seems very simple but can cause bad results if it is not known.  <br>

Firstly, let's immediately create a cluster with **k3d** 🏃

```
❯ k3d cluster create gitops-cluster-01 \
    -p "8080:30080@agent:0" \
    -p "8081:30081@agent:0" \
    --agents 2 --servers 1  \
    --image rancher/k3s:v1.27.4-k3s1
```

As we know from previous articles, we can copy from [here](../03-handling-multiple-applications-in-argo-cd/#argo-cd-installation), let's install the Argo CD in our cluster. 

I created a Root App like this and told it to look in the directory named `04-rename-appset` in the GitLab repository

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: root-app
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    namespace: default
    name: in-cluster
  project: default
  source:
    path: 04-rename-appset
    repoURL: https://gitlab.com/emreberber/manifests.git
    targetRevision: HEAD
```


I would like to deploy the guestbook application from the [argocd-example-apps](https://github.com/argoproj/argocd-example-apps) repository.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: ApplicationSet
metadata:
  name: guestbook-appset
  namespace: argocd
spec:
  generators:
  - clusters: {}
  template:      
    metadata:
      name: 'guestbook'
    spec:
      # The project the application belongs to.
      project: default
      
      # Source of the application manifests
      source:
        repoURL: https://github.com/argoproj/argocd-example-apps.git
        targetRevision: HEAD
        path: ./guestbook
      
      # Destination cluster and namespace to deploy the application
      destination:
        server: https://kubernetes.default.svc
        namespace: guestbook

      # Sync policy
      syncPolicy:
        syncOptions:
          - CreateNamespace=true  
        automated: # automated sync by default retries failed attempts 5 times with following delays between attempts ( 5s, 10s, 20s, 40s, 80s ); retry controlled using `retry` field.
          prune: true # Specifies if resources should be pruned during auto-syncing ( false by default ).
          selfHeal: true # Specifies if partial app sync should be executed when resources are changed only in target Kubernetes cluster and no git change detected ( false by default ).
      
```

Wait for the application to deploy and check.

![01-deploy-guestbook](img/04-rename-appset/01-deploy-guestbook.png)

I don't like the name ApplicationSet and would like to update :) 

❯ [Commit](https://gitlab.com/emreberber/manifests/-/commit/d1a14c79f7a6556d366b9dfce6df508fdbde730e)


![02-rename-appset](img/04-rename-appset/02-rename-appset.png)

It tells me that I am deleting the existing appset and I will create a new appset with a new name. For this, I delete the old one from the UI.


![03-new-app](img/04-rename-appset/03-new-app.png)

As you can see, k8s objects have been deleted and recreated. It was a situation we didn't want. Imagine that you are operating in an application running in a production environment, where not one but thousands of our applications are connected.

Now let's see how we can do this without any down time, without affecting our pods. Back to the old ApplicationSet name. It's quite easy to do this.

❯ [Commit](https://gitlab.com/emreberber/manifests/-/commit/7234ad142bea5498abd452b3c012b1cb945efaba)


{{< alert "check" >}} [Prevent an Application's child resources from being deleted, when the parent Application is deleted](https://argo-cd.readthedocs.io/en/stable/operator-manual/applicationset/Controlling-Resource-Modification/#prevent-an-applications-child-resources-from-being-deleted-when-the-parent-application-is-deleted) {{< /alert >}}


```yaml
syncPolicy:
    preserveResourcesOnDeletion: true
```

From now on, make sure this change is reflected in the Argo CD,

![04-add-flag](img/04-rename-appset/04-add-flag.gif)


It is safe to update the ApplicationSet name after this time.

❯ [Commit](https://gitlab.com/emreberber/manifests/-/commit/01e9597cabcd6fb6695a7f48df1df07c4eac4df8)



We can delete the one with the old name. You can do this with UI or CLI


![05-rename-appset](img/04-rename-appset/05-rename-appset.gif)


```shell
❯ kubectl get pods -n guestbook
NAME                            READY   STATUS    RESTARTS   AGE
guestbook-ui-6b7f6d9874-wnldj   1/1     Running   0          55m
```

That's it, we have safely updated the name and we have not lost anything. Finally, we can remove this flag.

❯ [Commit](https://gitlab.com/emreberber/manifests/-/commit/6510b1255596d7e52e49d7caf318478c71b9188e)


See you 🙋‍♂️